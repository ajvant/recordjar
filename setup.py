from setuptools import setup, find_packages

deps = []
setupdeps = ['setuptools-scm>=3.3.0']
name = 'recordjar'

setup(
    name=name,
    packages=find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    install_requires=deps,
    setup_requires=setupdeps,
    description='Read and write recordjar files',
    zip_safe=False,
    entry_points={'console_scripts': [f'{name} = {name}.cli:main']},
    use_scm_version={'write_to': f'src/{name}/version.py'},
)
