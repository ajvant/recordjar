# recordjar

A library for reading and writing record-jar-style file formats, as
described in [The Art of Unix Programming][taup].

## Usage

Each record-jar record consists of an arbitrary number of key-value
pairs ("metadata"), optionally followed by unstructured text
("content"). A customizable separator line delimits records. By default,
the expected format looks like this:

```
key1: something
key2: something else

this is a line of unstructured text
this is a second line of same
%%
keywhatever: who knows?

this is a second record
```

By default, metadata keys must consist of Unicode word characters, and
are separated from values by `: `. The metadata "block" is separated
from content by a blank line.

The default record separator is `%%`. Anything on the same line as the
record separator is ignored. Both the record separator and the key-value
separator can be overridden.

Assume for the moment that we have a file with the same content as the
example above:

```py
import recordjar

with open('filename.txt') as f:
    jar = recordjar.load(f)

print(len(jar))             # output: "2"
print(jar[0].meta['key1'])  # output: "something"
print(jar[1].text)          # output: "this is a second record"
```

Records have two properties. `Record.meta` contains the record's
key-value pairs, as a dictionary. All values are strings; no type
conversion is performed. `Record.text` contains the record's
unstructured content as a single string. Newlines in unstructured
content are preserved.

Alongside `recordjar.load`, there is also `recordjar.dump`. You can use
the two to convert between different record-jar formats.

## Customization

You can tweak the expected file format somewhat. This example uses
the equals sign as the key-value delimiter, and triple dashes as the
record delimiter:

```
key1=val1

first record text
---
key1=val2

second record text
```

```py
with open('filename.txt') as f:
    jar = recordjar.load(key_separator='=', record_separator='---')

print jar[0].meta['key1']  # output: "val1"
print jar[1].text          # output: "second record text"
```

## CLI

A small CLI frontend ships with the library, as a proof of concept. It
converts records from one jar format to another. See `recordjar --help`
for more details.

The frontend's source may be a useful reference when writing similar
tools.

## Limitations

### Delimiter Escaping

There is no provision for escaping the delimiter (yet). Choose a
delimiter you are sure won't show up in record content or key names --
at least not at the beginning of a line.

### Empty Metadata

recordjar tries to detect if a record has no metadata, only content. The
detection isn't perfect. It looks at the start of the first line for a
series of Unicode word characters, followed by the key separator (the
default regex is `'\w+' + key_separator`, but can be overridden). If the
line doesn't match, it assumes the record only has unstructured text
content.

That means you can't have an unstructured-text-only record where the
first line "looks like" a key-value pair. Instead, use a blank line as
the first line, to explicitly indicate that there is no metadata.

I dislike this situation and the detection feature will likely change.

[taup]: http://www.catb.org/~esr/writings/taoup/html/ch05s02.html#id2906931
