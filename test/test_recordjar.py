# pylint: disable=missing-docstring,wildcard-import

import unittest
from io import StringIO
from os.path import dirname
from os.path import join as pathjoin
from textwrap import dedent

from recordjar import *

MINIMAL_RECORD = dedent("""\
    key: value

    this is text
    """)

MINIMAL_JAR = dedent("""\
    index: 0

    text of record 0
    %%
    index: 1

    text of record 1
    """)


def realpath(filename):
    return pathjoin(dirname(__file__), f'files/{filename}')


def read(filename):
    with open(realpath(filename)) as f:
        return f.read()


class TestRecordFormat(unittest.TestCase):
    def setUp(self):
        self.fmt = RecordFormat()

    def test_eor(self):
        self.assertTrue(self.fmt.is_eor("%%\n"))
        self.assertFalse(self.fmt.is_eor("not a separator\n"))

    def test_eor_comments(self):
        self.assertTrue(self.fmt.is_eor("%% comment\n"))

    def test_blank(self):
        for s in ["\n", "    \n", "\t\n"]:
            self.assertTrue(self.fmt.is_blank(s))
        self.assertFalse(self.fmt.is_blank("not a blank"))

    def test_keyval(self):
        self.assertTrue(self.fmt.is_keyval("key: val\n"))
        self.assertFalse(self.fmt.is_keyval("not a keyval\n"))


class TestRecordJar(unittest.TestCase):
    def setUp(self):
        self.minimal = Jar.parse(MINIMAL_JAR)

    def test_parse(self):
        self.assertEqual(len(self.minimal), 2)

    def test_format(self):
        self.assertEqual(self.minimal.format(), MINIMAL_JAR)

    def test_str(self):
        self.assertEqual(str(self.minimal), MINIMAL_JAR)


class TestRecord(unittest.TestCase):
    def test_create(self):
        meta = {'key': 'value'}
        text = 'this is text'
        record = Record(text, meta)
        self.assertIsInstance(record, Record)
        self.assertEqual(record.text, text)
        self.assertEqual(record.meta, meta)

    def test_create_with_kwargs(self):
        meta = {'key': 'value'}
        text = 'this is text'
        record = Record(text, **meta)
        self.assertEqual(record.meta, meta)

    def test_parse(self):
        content = dedent("""\
            key: value

            this is text
            """)
        record = Record.parse(content)
        self.assertIsInstance(record, Record)
        self.assertEqual(record.meta['key'], "value")
        self.assertEqual(record.text, "this is text\n")

    def test_parse_lines(self):
        content = MINIMAL_RECORD.splitlines(True)
        record = Record.parse(content)
        self.assertEqual(record.meta['key'], "value")
        self.assertEqual(record.text, "this is text\n")

    def test_only_record(self):
        content = "this is text\n"
        record = Record.parse(content)
        self.assertEqual(record.text, content)
        self.assertEqual(record.meta, {})

    def test_broken_record(self):
        content = dedent("""\
            key: value
            this is text
            """)
        self.assertRaises(ValueError, Record.parse, content)

    def test_format(self):
        content = MINIMAL_RECORD
        record = Record.parse(content)
        self.assertEqual(record.format(), content)

    def test_str(self):
        content = MINIMAL_RECORD
        record = Record.parse(content)
        self.assertEqual(str(record), content)


class TestReader(unittest.TestCase):
    def setUp(self):
        self.instream = StringIO(MINIMAL_JAR)
        self.outstream = StringIO()

    def test_read(self):
        records = list(Reader(self.instream))
        self.assertEqual(len(records), 2)
        self.assertEqual(records[0].meta['index'], '0')
        self.assertEqual(records[1].text, 'text of record 1\n')

    def test_stream_check(self):
        self.assertRaises(ValueError, Reader, MINIMAL_JAR)

    def test_filename_suggestion(self):
        with self.assertRaises(ValueError) as context:
            Reader('filename.txt')
        self.assertIn('filename', str(context.exception))

        with self.assertRaises(ValueError) as context:
            Reader(MINIMAL_JAR)
        self.assertNotIn('filename', str(context.exception))


class TestWriter(unittest.TestCase):
    def setUp(self):
        self.jar = Jar.parse(MINIMAL_JAR)
        self.rec = Record.parse(MINIMAL_RECORD)
        self.outstream = StringIO()

    def test_write_one(self):
        writer = Writer(self.outstream)
        writer.write(self.rec)
        self.assertEqual(self.outstream.getvalue(), MINIMAL_RECORD)

    def test_write_all(self):
        writer = Writer(self.outstream)
        writer.write_all(self.jar)
        self.assertEqual(self.outstream.getvalue(), MINIMAL_JAR)


class TestHelpers(unittest.TestCase):
    def test_load(self):
        jar = load(StringIO(MINIMAL_JAR))
        self.assertEqual(len(jar), 2)
        self.assertEqual(jar[0].meta['index'], '0')
        self.assertEqual(jar[1].text, 'text of record 1\n')

    def test_dump(self):
        original = MINIMAL_JAR
        jar = load(StringIO(original))
        out = StringIO()
        dump(out, jar)
        self.assertEqual(out.getvalue(), original)
