#!/usr/bin/python3

""" Convert between concrete recordjar formats """

from .cli import main

if __name__ == '__main__':
    main()
