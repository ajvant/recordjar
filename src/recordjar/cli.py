#!/usr/bin/python3

""" CLI frontend for recordjar """

import sys
import logging
from logging import DEBUG, INFO, WARN
from argparse import ArgumentParser, FileType
from functools import partial

from . import Reader, Writer
from .version import version

log = logging.getLogger(__name__)


def main(argv=None):
    """ Entry point for recordjar """
    if argv is None:
        argv = sys.argv[1:]

    desc = "convert between recordjar formats"
    parser = ArgumentParser(description=desc)
    # Convenience aliases
    addarg = parser.add_argument
    addflag = partial(addarg, action='store_true')
    addsep = partial(addarg, metavar='SEP')

    addarg('infile', nargs='?', type=FileType('r'), default=sys.stdin)
    addarg('outfile', nargs='?', type=FileType('w'), default=sys.stdout)

    addsep('--irs', help='input record separator')
    addsep('--ors', help='output record separator')
    addsep('--iks', help='input key separator')
    addsep('--oks', help='output key separator')
    addsep('--ikf', metavar='regex', help='input metadata key format')
    addsep('--okf', metavar='regex', help='output metadata key format')

    addflag('-v', '--verbose', help='verbose output')
    addflag('-D', '--debug', help='debug output')
    addflag('-V', '--version', help='print program version')

    args = parser.parse_args(argv)

    if args.version:
        print(version)
        sys.exit(0)

    loglevel = (DEBUG if args.debug
                else INFO if args.verbose
                else WARN)
    logging.basicConfig(level=loglevel)

    reader = Reader(args.infile,
                    record_separator=args.irs,
                    key_separator=args.iks,
                    key_format=args.ikf)
    writer = Writer(args.outfile,
                    record_separator=args.ors,
                    key_separator=args.oks,
                    key_format=args.okf)
    writer.write_all(reader)


if __name__ == '__main__':
    main()
