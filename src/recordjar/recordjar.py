""" Recordjar library main module """

import logging
import re
import io
from itertools import takewhile
from collections.abc import Iterator
from collections import UserList
from types import SimpleNamespace

log = logging.getLogger(__name__)


class RecordFormat:
    """ A recordjar format

    This stores the separators and other options for a concrete
    recordjar format. Most functions in this module that require this
    information will accept either a RecordFormat or kwargs for creating one.
    """
    re_blank = re.compile(r'\s*$')
    aliases = {'true': True, 'yes': True, 'on': True,
               'false': False, 'no': False, 'off': False}

    defaults = SimpleNamespace()
    defaults.kfmt = r'\w+'
    defaults.ksep = ': '
    defaults.rsep = '%%'

    def __init__(self, has_text=True, key_format=None,
                 key_separator=None, record_separator=None):

        self.has_text = has_text
        self.key_format = key_format or self.defaults.kfmt
        self.key_separator = key_separator or self.defaults.ksep
        self.record_separator = record_separator or self.defaults.rsep

        self.re_meta = re.compile(self.key_format + self.key_separator)

    def is_eor(self, line):
        """ Check if a line is the end of the record """
        return line.startswith(self.record_separator)

    def is_keyval(self, line):
        """ Check if a line could be a key/value pair

        Used for detecting if the first line is metadata or payload
        """
        return self.re_meta.match(line)

    def is_blank(self, line):
        """ Check if a line is blank """
        return self.re_blank.match(line)


def _validate_stream(stream):
    """ check that stream is actually a stream and complain if not """
    if isinstance(stream, io.TextIOBase):
        return
    msg = f"expected a stream, got a {type(stream).__name__}"
    if isinstance(stream, str) and '\n' not in stream:
        msg += " (did you pass a filename instead of a file?)"
    raise ValueError(msg)


class Reader(Iterator):  # pylint: disable=too-few-public-methods
    """ Read records from an input stream

    The interface is similar to that of the standard library's csv.reader
    """

    def __init__(self, stream, fmt=None, **kwargs):
        _validate_stream(stream)
        self.stream = stream
        self.fmt = fmt or RecordFormat(**kwargs)

    def __next__(self):
        record_lines = takewhile(lambda l: not self.fmt.is_eor(l), self.stream)
        content = ''.join(record_lines)
        if not content:
            raise StopIteration
        return Record.parse(content, self.fmt)


class Writer:
    """ Write records to an output stream

    The interface is similar to that of the standard library's csv.writer.
    """
    def __init__(self, stream, fmt=None, **kwargs):
        _validate_stream(stream)
        self.stream = stream
        self.fmt = fmt or RecordFormat(**kwargs)

    def write(self, record):
        """ Write a single record the output stream """
        self.stream.write(record.format(self.fmt))

    def write_separator(self):
        """ Write the separator to the output stream """
        self.stream.write(self.fmt.record_separator + '\n')

    def write_all(self, records):
        """ Write all records in an iterable to the output stream """
        for i, record in enumerate(records):
            if i > 0:
                self.write_separator()
            self.write(record)


class Jar(UserList):  # pylint: disable=too-many-ancestors
    """ A record jar

    Jar is a list-like object with added parsing/formatting methods
    """
    default_format = RecordFormat()

    def __str__(self):
        return self.format()

    def format(self, fmt=None):
        """ Format a record with the given RecordFormat """
        if fmt is None:
            fmt = self.default_format
        out = io.StringIO()
        writer = Writer(out)
        writer.write_all(self)
        return out.getvalue()

    @classmethod
    def parse(cls, content, fmt=None):
        """ Parse an entire recordjar with the given RecordFormat """
        if isinstance(content, str):
            content = io.StringIO(content)
        reader = Reader(content, fmt)
        return cls(reader)


class Record:
    """ A recordjar record

    Records have two public attributes:

    meta: a dictionary of the record's structured data
    text: a string of the record's unstructured text part, if present
    """
    default_format = RecordFormat()

    def __init__(self, text, meta=None, **kwargs):
        self.text = text
        self.meta = meta or {}
        self.meta.update(kwargs)

    def __str__(self):
        return self.format()

    def format(self, fmt=None):
        """ Format a record with the given RecordFormat """
        if fmt is None:
            fmt = self.default_format
        out = io.StringIO()
        for k, v in self.meta.items():
            sep = fmt.key_separator
            out.write(f'{k}{sep}{v}\n')
        if self.text is not None:
            out.write('\n')
            out.write(self.text)
        return out.getvalue()

    @classmethod
    def parse(cls, content, fmt=None):
        """ Parse a record using the given RecordFormat

        `content` may be either a single string containing the entire record,
        or a list of lines. If lines, they must end with a newline.
        """

        if fmt is None:
            fmt = cls.default_format
        if isinstance(content, str):
            lines = content.splitlines(True)  # newlines need to stay
        else:
            lines = content

        meta_lines = []
        text_lines = []
        section = meta_lines

        log.debug("starting read")
        for i, line in enumerate(lines):
            if i == 0 and not fmt.is_keyval(line):
                log.debug("no meta on first line, skipping meta")
                section = text_lines
            elif section is meta_lines and fmt.is_blank(line):
                log.debug("metadata ended at line %s (line is blank)", i)
                section = text_lines
                continue
            log.debug("adding line: %s", line)
            section.append(line)

        text = ''.join(text_lines)
        meta = dict()

        for line in meta_lines:
            line = line.rstrip('\n')
            try:
                k, v = line.split(fmt.key_separator)
            except ValueError:
                msg = 'Malformed record (no blank between keys and text?)'
                raise ValueError(msg)
            meta[k] = fmt.aliases.get(v.lower(), v)

        return cls(text, meta)


def load(stream, **fmtargs):
    """ Populate a new recordjar from a stream """
    fmt = RecordFormat(**fmtargs)
    reader = Reader(stream, fmt)
    return Jar(reader)


def dump(stream, jar, **fmtargs):
    """ Write a recordjar to a stream """
    fmt = RecordFormat(**fmtargs)
    writer = Writer(stream, fmt)
    writer.write_all(jar)
